package com.tgl.mvc.dao;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.tgl.mvc.mapper.EmployeeRowMapper;
import com.tgl.mvc.model.Employee;

@Repository
public class EmployeeDao {

  private static final Logger LOG = LogManager.getLogger(EmployeeDao.class);

  private static final String INSERT =
      "INSERT INTO employee (NAME, ENNAME, EMAIL, TALL, WEIGHT, BMI, PHONE) VALUES (:Name, :enName, :email, :tall, :weight, :bmi, :phone)";

  private static final String DELETE = "DELETE FROM employee WHERE ID=:id";

  private static final String UPDATE =
      "UPDATE employee SET NAME=:Name, ENNAME=:enName, EMAIL=:email, TALL=:tall, WEIGHT=:weight, BMI=:bmi, PHONE=:phone WHERE ID=:id";

  private static final String SELECT =
      "SELECT ID, NAME, ENNAME, EMAIL, TALL, WEIGHT, BMI, PHONE FROM employee";

  @Autowired
  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  public long insert(Employee employee) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    namedParameterJdbcTemplate.update(INSERT, new BeanPropertySqlParameterSource(employee),
        keyHolder);
    return keyHolder.getKey().longValue();
  }

  @CacheEvict(value = "empFindById", key = "#employeeId")
  public boolean delete(long employeeId) {
    return namedParameterJdbcTemplate.update(DELETE,
        new MapSqlParameterSource("id", employeeId)) > 0;
  }

  @CachePut(value = "empFindById", key = "#employee.id")
  public Employee update(Employee employee) {
    namedParameterJdbcTemplate.update(UPDATE,
        new BeanPropertySqlParameterSource(employee));
    return employee;
  }

  @Cacheable(value = "empFindById", key = "#employeeId")
  public Employee findById(long employeeId) {
    try {
      return namedParameterJdbcTemplate.queryForObject(SELECT + " WHERE id=:id",
          new MapSqlParameterSource("id", employeeId), new EmployeeRowMapper());
    } catch (EmptyResultDataAccessException e) {
      LOG.error("EmployeeDao findById failed, id: {}, Exception: {}", employeeId, e);
    }
    return null;
  }
}